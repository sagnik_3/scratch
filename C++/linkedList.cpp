//implementation of singly likned list in c++

#include <iostream>
#include <vector>
#include <cassert>
#include <cstddef>


using namespace std;

//single node if the list
struct Node {
    //every node will have a node to the next one
    int data;
    Node *next; //node to the next

    //constructor
    Node(int d) {
        data = d;
        next = NULL;
    }
};

class LinkedList {
  private:
    //pointer to the list head
    Node *head;

  public:
    //construtor init the NULL pointers
    LinkedList() {
        head = NULL;
    }
    //add node with data 'd' at head pons
    void insert_head(int d);

    //addd node with data 'd' at posn in list 'p'
    void insert_position(int d, int p);

    //add node with data 'd' at tail posn
    void insert_tail(int d);

    //delete node at head posn
    void delete_head();
    //delete node at posn in the list 'p'
    void delete_position(int p);
    //delete node at tail position
    void delete_tail();

    //print out the list
    void print_list();
};


//1. addin to the head of the list

void LinkedList::insert_head(int d) {
    //craete a new node with data value 'd'
    Node *new_head = new Node(d);
    //insert at the front of the list
    new_head->next = head;
    head = new_head;

    cout << "Head inserted with data: " << d << "\n";
    print_list();
}

//add node at posn 'p' in the list
// Position 'p' must exist in the list
void LinkedList::insert_position(int d , int p) {
    //craete a new node with data value 'd'
    Node *new_node = new Node(d);

    //setting the head = new_node if the list is empty and p ==0
    //else perform normal insertion
    if (head == NULL) {
        //check if the head insertion happened
        //else it is a invald operation
        if (p == 0) {
            head = new_node;
        } else {
            //insert into empty list at 20 th element eg.
            assert(head == NULL && p == 0);
        }
    } else {
        //walk the list to get to the entry point for the new node
        Node *temp = head ;
        Node *prev = NULL ;
        for (int i = 0; i < p; i++) {
            //check if the inserttion point is past potential new tail
            assert(temp != NULL);
            //update the previous node and the current node
            prev =  temp;
            temp = temp->next; //point to the new node
        }
        //chek if this is actully head insertion
        //otherwise perform normal insertion
        if (prev == NULL ) {
            head = new_node;
            head->next = temp;
        } else {
            new_node->next = prev->next;//somewhere along the list and somewhere overlapped
            //witht eh previous node and set newnode what previous was to
            prev->next = new_node;
        }
    }
    cout << "Postion " << p << " insert with data: " << d << "\n";
    print_list();

}

//add node at tail
void LinkedList::insert_tail(int d) {
    //create new noded with value 'd'
    Node* new_tail = new Node(d);

    //handle case of the empty list
    // otherwise perform normal insertion
    if (head == NULL ) {
        head = new_tail;
    } else {
        //create temp for wlaking the list
        Node *temp = head;

        //walk the list until the next node is NULL
        while (temp->next != NULL) {
            temp = temp->next;
        }
        //assign the previous tail's ,'next' to be our new node
        temp->next = new_tail;
    }
    cout << "Tail insert with data : " << d << "\n";
    print_list();
}

//deleting the head
void LinkedList::delete_head() {
    //check if the list is empty
    assert(head != NULL);

    //set head->start as new new head and free the old one
    Node* temp = head;
    head = temp->next;
    delete temp;

    cout << "Head deleted " << endl;
    print_list();
}


//delrte node at posn 'p' in list
//assume 'p' is a valid location

void LinkedList::delete_position(int p) {
    //chek if the list is empty
    assert(head != NULL );
    //walk the list to the posn for deleteion
    Node *temp = head ;
    Node *prev = NULL ;

    for (int i = 0; i < p; i++) {
        //check if the deletion point is past the end of the list
        assert(temp != NULL);
        prev = temp ;
        temp = temp->next;
    }
    //handle head deletion
    //otherwise normal deletion
    if (prev == NULL ) {//deleting the first item
        head = temp->next;
    } else {
        prev->next = temp->next;
    }
    delete temp;
    cout << "Position " << p << " is deleted " << endl;
    print_list();
}

//delete node at tail of list
void LinkedList::delete_tail() {
    //check if the list is empty
    assert(head != NULL );

    //walk the lsit and save the previous and current Node
    Node *temp = head ;
    Node *prev = NULL ;
    while (temp->next != NULL) {
        prev = temp;
        temp = temp->next;
    }
    //handle case where there is only 1 list item
    // otherwise normal tail delete
    if (prev == NULL) {
        head = NULL;
    } else {
        prev->next  = temp->next;
    }
    delete temp ;
    cout << "Tail deleted " << endl;
    print_list();
}


//print the list
void LinkedList::print_list() {
    for (int i = 0; i <= 72; i++) {
        cout << "-";
    }
    cout << endl;

    Node* temp = head ;
    cout << "List:-\t";
    while (temp != NULL) {
        cout << temp->data << "\t";
        temp = temp->next;
    }
    cout << endl;

    for (int i = 0; i < 72; i++) {
        cout << "-";
    }
    cout << endl;
}



int main() {
    //new linked list instance
    LinkedList lt ;

    //insert a few nodes,alternating at head and tail
    for (int i = 0; i < 5; i++) {
        if (i % 2) {
            //insert at head at odd numbers
            lt.insert_head(i);
        } else {
            //insert at tail at even numbers
            lt.insert_tail(i);
        }
    }

    //insert at the beginning of the list
    lt.insert_position(5, 2);
    //delete the elemnt we just put in
    lt.delete_position(2);

    //delete the hed
    lt.delete_head();

    //delet the tail
    lt.delete_tail();

    return 0;

}
